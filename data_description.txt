mass_npea:         the mass of the area understudy for melanoma tumor
size_npear:        the size of the area understudy for melanoma tumor
malign_ratio:      ration of normal to malign surface understudy
damage_size:       unrecoverable area of skin damaged by the tumor
exposed_area:      total area exposed to the tumor
std_dev_malign:    standard deviation of malign skin measurements
err_malign:        error in malign skin measurements
malign_penalty:    penalty applied due to measurement error in the lab
damage_ratio:      the ratio of damage to total spread on the skin
tumor_size:        size of melanoma_tumor

se describe el dataset muy por encima al leerse este
isolation forest https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.IsolationForest.html
paper isolation forest: F. T. Liu, K. M. Ting and Z. Zhou, "Isolation Forest," 2008 Eighth IEEE International Conference on Data Mining, 2008, pp. 413-422, doi: 10.1109/ICDM.2008.17.

comparacion algoritmo de anomalos https://scikit-learn.org/stable/auto_examples/miscellaneous/plot_anomaly_comparison.html#sphx-glr-auto-examples-miscellaneous-plot-anomaly-comparison-py

cross validate https://scikit-learn.org/stable/modules/cross_validation.html#
train test split https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.train_test_split.html#sklearn.model_selection.train_test_split
MSE https://scikit-learn.org/stable/modules/generated/sklearn.metrics.mean_squared_error.html#sklearn.metrics.mean_squared_error
linear regression https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.LinearRegression.html#sklearn.linear_model.LinearRegression
normalize https://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.normalize.html
